var inputs = $('.bt-input input');
inputs.on('input', function(idx) {
$(inputs[inputs.index(this)]).toggleClass('active', this.value > '');
});
var lng_flag =  document.getElementById("lng-flag");

window.onload = function() {
    if ( localStorage.getItem("emailAutomation_language_key") === null ) {
        console.log(" key null 2    ")
        localStorage.setItem("emailAutomation_language_key", "en");
    }
    load()
    lng_flag.src = `images/${localStorage['emailAutomation_language_key']}.jpg`;
};

function getSignPage (){
    $("#login_page").addClass("remove_slide");
    $("#signUp_page").addClass("active");
}
function getLoginPage(){
    $("#login_page").removeClass("remove_slide");
    $("#signUp_page").removeClass("active");
}



function changeLng(key) {
    localStorage["emailAutomation_language_key"] = key ;
    lng_flag.src = `images/${localStorage['emailAutomation_language_key']}.jpg`;
    load();
    console.log("working change")  
}

function load(){
    var translate = new Translate();
    var currentLng =  localStorage.getItem("emailAutomation_language_key");
    console.log( "crr " + currentLng);
    var attributeName = 'data-tag';
    translate.init(attributeName, currentLng);
    translate.process(); 
}

 function Translate() { 
	//initialization
	this.init =  function(attribute, lng){
		this.attribute = attribute;
		this.lng = lng;	
	}
	//translate 
	this.process = function(){
        _self = this;
        var xrhFile = new XMLHttpRequest();
        //load content data 
        // xrhFile.open("GET", "./resources/"+this.lng+".json", false);
        // https://www.qandle.com/images/lp/lan/resources/en.json
        xrhFile.onreadystatechange = function ()
        {
            if(xrhFile.readyState === 4)
            {
                if(xrhFile.status === 200 || xrhFile.status == 0)
                {
                    console.log("api success");
                    var LngObject = JSON.parse(xrhFile.responseText);
                    // console.log(LngObject["name1"]);
                    var allDom = document.getElementsByTagName("*");
                    for(var i =0; i < allDom.length; i++){
                        var elem = allDom[i];
                        var key = elem.getAttribute(_self.attribute);
                            
                        if(key != null) {
                                // console.log(key);
                                elem.innerHTML = LngObject[key]  ;
                        }
                    }
                
                }
            }
        }
        xrhFile.send();
    }
	
}

