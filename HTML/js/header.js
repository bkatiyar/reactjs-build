document.write(`
<header class="header animated fadeInUpShort delay-250 go">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-light w-100">
                <a class="navbar-brand" href="index.html"><img src="images/logo.png" width="230" alt="icon"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="index.html#product_section" data-tag="nav_home">Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="pricing.html" data-tag="nav_pricing">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="faq.html" data-tag="nav_faq">FAQs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-tag="nav_pricing">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link bt-button  blue-border" href="login.html" data-tag="nav_login_in">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link bt-button bg-blue-dark  blue-border white" href="login.html" data-tag="nav_sign_up">Sign Up</a>
                    </li>
                    <li class="nav-item">
                        <div class="dropdown header-lng-d">
                            <a class="nav-link header-lng-btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="header-lab-st align-center flex">
                                    <span><img id="lng-flag" src="images/en.jpg" alt="icon"/></span>
                                    <span class="material-icons"> keyboard_arrow_down</span>
                                </div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" onClick="changeLng('en')">
                                    <div class="header-lab-st">
                                        <span><img src="images/en.jpg" alt="icon"/></span>    
                                        <span>EN</span>
                                        
                                    </div>
                                </a>
                                <a class="dropdown-item" onClick="changeLng('pl')">
                                    <div class="header-lab-st">
                                        <span><img src="images/pl.jpg" alt="icon"/></span>    
                                        <span>PL</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
`)