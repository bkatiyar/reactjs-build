document.write(`
<footer class="sec-are startGrowingst bg-blue2 animatedParent">
    <div class="container">
      <div class="row">
        <div class="col-md-6 animated fadeInUpShort delay-500 br1">
            <h3 class="bt-heading white mt-5" data-tag="home_growing_withfollowps">Put your email follow-ups <br>on auto pilot </h3>
            <a href="login.html" class="mt-4 bt-button bg-white black border-white btn-md bt-auto" data-tag="home_getStart">Get started</a>
        </div>
        <div class="col-md-6 animated fadeInUpShort delay-500" style="display: flex;align-items: center;">
          <h3 class="bt-heading fs-24 white mt-5" data-tag="home_growing_withfollowps">London | Warsaw | Hong Kong | Delhi NCR <br> Contact us help@followups.wtf </h3>
        </div>
      </div>

    </div>
</footer>
<div class="footer_tC_link"> <p> <a href="privacy_policy.html">Privacy Policy</a> | <a href="terms_conditions.html">Terms & Conditions</a></p></div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" data-tag="model_contact_our_team">Contact our  Team</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="login-formst">
            <div class="bt-input fa">
              <input type="text">
              <label data-tag="model_form_name">Name</label>
            </div>
            <div class="bt-input fa">
              <input type="text">
              <label data-tag="Model_form_mobile">Mobile Number</label>
            </div>
            <div class="mt-3 text-center">
              <button class="bt-primary" data-tag="model_submit">Submit</button>
            </div>
          </div>
      </div>
      
    </div>
  </div>
</div>

`)